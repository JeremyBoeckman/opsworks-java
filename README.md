# README #

This is a customised template and cookbook to get Opsworks up and running with Java 8 and Tomcat 8. This also has fix for disabling SSL v3 on the fronting Apache. 

## Introduction ##

* This repository has two purposes
* Apache Changes
* Java Changes

### Apache Changes ###

* Disables SSL v3

### Java Layer Changes ###

* Support for Java 8
* Support for Tomcat 8
* Sets the Java version to 8 and tomcat version to 8
* Add -Djava.awt.headless as a default
* Tested only on Amazon Linux (2015.03)

### Known Issues ###

* JAVA_OPTS are duplicated while running tomcat. That is, -Djava.awt.headless and other parameters provided in the stack appear twice in the command line

### How do I get set up? ###

Best thing would be to fork this onto your repo and provide that URL as the custom chef script URL. If you are new to Opsworks, please read the following:

* [Overriding AWS Opsworks...](http://docs.aws.amazon.com/opsworks/latest/userguide/workingcookbook-cookbook-attributes.html)
* [Basic Ruby for Chef](https://docs.chef.io/ruby.html)
* [Chef documentation](http://docs.chef.io/resources.html)

### Contribution guidelines ###

* Feel Free to fork this and make changes. You can contact me at the address below. 

### Who do I talk to? ###

* Contact me at razee (at) aisle (dot) co (NOT COM)